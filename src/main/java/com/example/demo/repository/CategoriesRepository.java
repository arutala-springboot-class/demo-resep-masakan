package com.example.demo.repository;

import com.example.demo.model.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoriesRepository extends JpaRepository<Categories, Long> {
    List<Categories> findAll();
    List<Categories> findByIsDeleted(Boolean isDeleted);

    @Query(value = "select * from categories e where LOWER(e.category_name) LIKE %:name%", nativeQuery = true)
    List<Categories> findByCategoriesName(String name);


}